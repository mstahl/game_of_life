package main

import (
	"fmt"
	"os"
	"time"

	termbox "github.com/nsf/termbox-go"
)

var frameRate int

func main() {
	if err := termbox.Init(); err != nil {
		panic(err)
	}

	w, h := termbox.Size()
	g := NewGrid(w, h)

	go func() {
		for {
			switch ev := termbox.PollEvent(); ev.Type {
			case termbox.EventError:
				panic(ev.Err)
			case termbox.EventKey:
				fallthrough
			case termbox.EventInterrupt:
				termbox.Flush()
				termbox.SetCursor(1, 1)
				termbox.Close()
				os.Exit(0)
			}
		}
	}()

	var frameCount int
	go func() {
		for {
			time.Sleep(1 * time.Second)
			frameRate = frameCount
			frameCount = 0
		}
	}()

	for {
		g.Draw()
		g.Iterate()
		termbox.SetCursor(0, 0)
		fmt.Print(frameRate, "fps")
		termbox.Flush()
		time.Sleep((1000 / 30) * time.Millisecond)
		frameCount += 1
	}
}
