package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"sync"
	"time"

	"github.com/fatih/color"
	termbox "github.com/nsf/termbox-go"
)

var ColorFns = [...]func(...interface{}) string{
	color.New(color.FgRed).SprintFunc(),
	color.New(color.FgGreen).SprintFunc(),
	color.New(color.FgYellow).SprintFunc(),
	color.New(color.FgBlue).SprintFunc(),
	color.New(color.FgMagenta).SprintFunc(),
	color.New(color.FgCyan).SprintFunc(),
	color.New(color.FgWhite).SprintFunc(),
}

type Idx [2]int

func (i Idx) x() int {
	return i[0]
}

func (i Idx) y() int {
	return i[1]
}

func (i Idx) add(j Idx) Idx {
	return Idx{i[0] + j[0], i[1] + j[1]}
}

func (i Idx) String() string {
	return fmt.Sprintf("(%d, %d)", i.x(), i.y())
}

type Grid struct {
	m      map[Idx]bool
	width  int
	height int
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func NewGrid(w, h int) *Grid {
	g := &Grid{}
	g.width, g.height = w, h
	g.m = make(map[Idx]bool)

	for i := 0; i < w; i++ {
		for j := 0; j < h; j++ {
			g.m[Idx{i, j}] = rand.Float64() < 0.5
		}
	}
	return g
}

func (g *Grid) String() (str string) {
	for y := 0; y < g.height; y++ {
		for x := 0; x < g.width; x++ {
			if g.m[Idx{x, y}] {
				str += "x"
			} else {
				str += " "
			}
		}
		str += "\n"
	}
	return
}

// Sample returns a random index into the grid
var neighborIdxs = [8]Idx{
	{-1, -1}, {0, -1}, {1, -1},
	{-1, 0}, {1, 0},
	{-1, 1}, {0, 1}, {1, 1},
}

// NeighborsOf returns a slice of indexes of all the reachable neighbors of the
// given index
func (g *Grid) NumNeighborsOf(c Idx) (n int) {
	for _, ni := range neighborIdxs {
		if x, f := g.m[ni.add(c)]; f && x {
			n++
		}
	}
	return
}

// Applies the game of life rules to all the cells in the grid
func (g *Grid) Iterate() {
	tmpGrid := NewGrid(g.width, g.height)

	wg := &sync.WaitGroup{}
	numCPU := runtime.NumCPU()
	wg.Add(numCPU)
	rowsPerCPU := g.height / numCPU
	gRW := &sync.RWMutex{}   // Mutex for RW access to `g`
	tmpRW := &sync.RWMutex{} // Mutex for RW access to `tmp`

	for i := 0; i < numCPU; i++ {
		// Spawn a goroutine for each physical CPU to compute a band of the tmpGrid
		go func(_wg *sync.WaitGroup, _i int) {
			for y := _i * rowsPerCPU; y < (_i+1)*rowsPerCPU+numCPU; y++ {
				for x := 0; x < g.width; x++ {
					gRW.RLock()
					numNeighbors := g.NumNeighborsOf(Idx{x, y})
					s := g.m[Idx{x, y}]
					gRW.RUnlock()

					tmpRW.Lock()
					if s {
						switch {
						case numNeighbors < 2:
							tmpGrid.m[Idx{x, y}] = false
						case numNeighbors > 3:
							tmpGrid.m[Idx{x, y}] = false
						default:
							tmpGrid.m[Idx{x, y}] = true
						}
					} else if numNeighbors == 3 {
						tmpGrid.m[Idx{x, y}] = true
					} else {
						tmpGrid.m[Idx{x, y}] = false
					}
					tmpRW.Unlock()
				}
			}
			_wg.Done()
		}(wg, i)
	}
	wg.Wait()

	// Copy tmpGrid into grid atomically
	for y := 0; y < g.height; y++ {
		for x := 0; x < g.width; x++ {
			g.m[Idx{x, y}] = tmpGrid.m[Idx{x, y}]
		}
	}
}

func (g *Grid) Draw() {
	termbox.Clear(termbox.ColorBlack, termbox.ColorBlack)
	for y := 0; y < g.height; y++ {
		for x := 0; x < g.width; x++ {
			if g.m[Idx{x, y}] {
				termbox.SetCell(x, y, ' ', termbox.ColorBlack, termbox.ColorWhite)
			}
		}
	}
	termbox.Flush()
}
