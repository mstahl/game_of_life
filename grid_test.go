package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewGrid(t *testing.T) {
	testCases := []struct {
		x  int
		y  int
		ef bool
	}{
		{0, 0, true},
		{0, 1, true},
		{1, 0, true},
		{1, 1, true},
		{2, 2, false},
		{2, 0, false},
		{0, 2, false},
		{-1, 1, false},
		{1, -1, false},
		{5, 1, false},
		{1, 7, false},
		{-2, -3, false},
		{7, 13, false},
	}
	g := NewGrid(2, 2)
	for _, tc := range testCases {
		_, f := g.m[Idx{tc.x, tc.y}]
		assert.Equal(t, tc.ef, f)
	}
}

func TestNumNeighborsOf(t *testing.T) {
	g := NewGrid(3, 3)

	// Pattern 1: Set all the cells to true
	for x := 0; x < g.width; x++ {
		for y := 0; y < g.height; y++ {
			g.m[Idx{x, y}] = true
		}
	}

	testCases := []struct {
		x int
		y int
		n int
	}{
		{0, 0, 3},
		{0, 2, 3},
		{2, 0, 3},
		{2, 2, 3},
		{0, 1, 5},
		{1, 0, 5},
		{2, 1, 5},
		{1, 2, 5},
		{1, 1, 8},
	}

	for i, tc := range testCases {
		t.Logf("Pattern 1: Test %d %v", i, Idx{tc.x, tc.y})
		assert.Equal(t, tc.n, g.NumNeighborsOf(Idx{tc.x, tc.y}))
	}
}
